# codility

[Codility :](https://www.codility.com/) Tech Recruiting Platform, Remote Online Code Testing

## Branch

- [x] Python
- [x] PHP
- [ ] C/C++
- [ ] Java
- [ ] JavaScript
- [ ] Ruby
